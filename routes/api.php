<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// public endpoints
$router->get('/hello', function () {
    return ':)';
});



Route::middleware(['auth:api'])->group(function () {
    Route::get('/protected-endpoint', function () {
        return 'acceso concedido';
    }); 
});
